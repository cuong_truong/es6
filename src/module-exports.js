export var LUCKY_NUMBER = 666;
export var LUCKY_STRING = 'Black Clover';

export var LUCKY_FUNC = () => 'You are DOOMED!';

var NUMBER_ONE = 1;
var NUMBER_TWO = '2';
var NUMBER_THREE = 'ba';

export default {
  one: NUMBER_ONE,
  two: NUMBER_TWO,
  three: NUMBER_THREE
};

var NUMBER_FOUR = 4;
var NUMBER_FIVE = 5;
var NUMBER_SIX = 6;

export {
  NUMBER_FOUR,
  NUMBER_FIVE
};
