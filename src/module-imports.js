import Numbers, {
  LUCKY_NUMBER,
  LUCKY_STRING,
  LUCKY_FUNC,
  NUMBER_FOUR as four,
  NUMBER_FIVE as five,
  NUMBER_SIX
}
from './module-exports';

console.log(
  LUCKY_STRING, ' - ',
  LUCKY_NUMBER, ' - ',
  LUCKY_FUNC(), ' - ',
  four, ' - ',
  five, ' - ',
  NUMBER_SIX
);
console.log(Numbers);

