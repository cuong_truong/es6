console.log("\nDemo arrow function\n");
var book = {
  name: 1984,
  author: 'George Orwell',
  toVietnameseNameES5: function () {
    return 'Cuốn tiểu thuyết "1984" của George Orwell';
  },
  toVietnameseNameES6: () =>
    'Cuốn tiểu thuyết "1984" của George Orwell',
  toEnglishNameES5: function (bookName, bookAuthor) {
    return function() {
      return 'The novel "' + bookName + '" of ' + bookAuthor;
    };
  },
  toEnglishNameES6WithBraces: (bookName, bookAuthor) => {
    return () => {
      return 'The novel "' + bookName + '" of ' + bookAuthor;
    };
  },
  toEnglishNameES6WithoutBraces: (bookName, bookAuthor) =>
    () => 'The novel "' + bookName + '" of ' + bookAuthor,
  getBookRecordES5: function() {
    return {
      fullName: this.name + ' - ' + this.author,
      getStringES5: function() {
        return this.name + ' by ' + this.author;
      },
      getStringES6: () => this.name + ' by ' + this.author
    };
  },
  getBookRecordES6: () => ({
    fullName: this.name + ' - ' + this.author,
    getStringES5: function() {
      return this.name + ' by ' + this.author;
    },
    getStringES6: () => this.name + ' by ' + this.author
  })
};

console.log("book.name = ", book.name);
console.log("book.author = ", book.author);
console.log("book.toVietnameseNameES5() = ", book.toVietnameseNameES5());
console.log("book.toVietnameseNameES6() = ", book.toVietnameseNameES6());
console.log(
  "book.toEnglishNameES5(book.name, book.author)() = ",
  book.toEnglishNameES5(book.name, book.author)()
);
console.log(
  "book.toEnglishNameES6WithBraces(book.name, book.author)() = ",
  book.toEnglishNameES6WithBraces(book.name, book.author)()
);
console.log(
  "book.toEnglishNameES6WithoutBraces(book.name, book.author)() = ",
  book.toEnglishNameES6WithoutBraces(book.name, book.author)()
);

var bookRecordES5 = book.getBookRecordES5();
console.log("bookRecordES5.fullName = ", bookRecordES5.fullName);
console.log("bookRecordES5.getStringES5() = ", bookRecordES5.getStringES5());
console.log("bookRecordES5.getStringES6() = ", bookRecordES5.getStringES6());

var bookRecordES6 = book.getBookRecordES6();
console.log("bookRecordES6.fullName = ", bookRecordES6.fullName);
console.log("bookRecordES6.getStringES5() = ", bookRecordES6.getStringES5());
console.log("bookRecordES6.getStringES6() = ", bookRecordES6.getStringES6());



console.log("\nDemo defaults");
function addBook(name, author = 'V.A.', stringBuilderFunc = book.toEnglishNameES6WithoutBraces(name, author)) {
  console.log('Name: ', name, ' - Author: ', author);
  console.log('Built string: ', stringBuilderFunc());
  console.log('Arguments: ', arguments);
}

console.log("\nWithout all parameters");
addBook();
// Name:  undefined
// Author:  V.A.
// Built string:  The novel "undefined" of V.A.
// Arguments:  {}

console.log("\nWith only first parameters");
addBook(book.name);
// Name:  1984
// Author:  V.A.
// Built string:  The novel "1984" of V.A.
// Arguments:  { '0': 1984 }

console.log("\nWith only second parameters");
addBook(undefined, book.author);
// Name:  undefined
// Author:  George Orwell
// Built string:  The novel "undefined" of George Orwell
// Arguments:  { '0': undefined, '1': 'George Orwell' }

console.log("\nWith only third parameters");
addBook(undefined, undefined, book.toVietnameseNameES5);
// Name:  undefined
// Author:  V.A.
// Built string:  Cuốn tiểu thuyết "1984" của George Orwell
// Arguments:  { '0': undefined, '1': undefined, '2': [Function: toVietnameseName] }

console.log("\nWith first and second parameters as nulls");
addBook(null, null);
// Name:  null
// Author:  null
// Built string:  The novel "null" of null
// Arguments:  { '0': null, '1': null, '2': [Function] }

console.log("\nWith all parameters");
addBook(book.name, book.author, book.toVietnameseNameES6);
// Name:  1984  - Author:  George Orwell
// Built string:  Cuốn tiểu thuyết "1984" của George Orwell
// Arguments:  { '0': 1984, '1': 'George Orwell', '2': [Function: toVietnameseNameES6] }


// const addOne = x => x + 1;
// const sum = (x, y) => x + y;
// const getPi = () => 3.1416;
// const stringDecorator = (prefix, postfix) => string => prefix + string + postfix;
// const goalDecorator = stringDecorator('Goal: ', '$');

// console.log(goalDecorator(5000)); // => 'Goal: 5000$'

const addOne = function(x) { return x + 1; };
const sum = function(x, y) { return x + y; };
const getPi = function() { return 3.1416; }
const stringDecorator = function(prefix, postfix) {
  return function(string) {
    return prefix + string + postfix;
  };
};
const goalDecorator = stringDecorator('Goal: ', '$');

console.log(goalDecorator(5000)); // => 'Goal: 5000$'

console.log(addOne(5)); // => 6
console.log(sum(6,9)); // => 15
console.log(getPi()); // => 3.1416

