//Rest operator
const cold = ['autumn', 'winter', 'spring'];  
const warm = ['spring', 'summer'];  

const countArguments = (...args) => {  
   return args.length;
}
// get the number of arguments
console.log(countArguments('welcome', 'to', 'Earth'));

// destructure an array
const otherSeasons, autumn;  
[autumn, ...otherSeasons] = cold;
console.log(otherSeasons);

//Selective rest parameter
const filterInputType = (type, ...items) => {  
  return items.filter(item => typeof item === type);
}
console.log(filterInputType('boolean', true, 0, false));
console.log(filterInputType('number', false, 4, 'Welcome', 7)); 

//Spread operator
const arr = [1,2,3];
const someArr = [...arr, 4];
const thatArr = [4, ...arr, 5]; 

console.log(someArr);
console.log(thatArr);

//Copy Array
const arr = [1, 2, 3];
const newArr = [...arr];
newArr.push(4);

//Array destructure
const seasons = ['winter', 'spring', 'summer', 'autumn'];  
const [coldSeason, ...otherSeasons] = seasons;

console.log(coldSeason);   // => 'winter' 
console.log(otherSeasons); // => ['spring', 'summer', 'autumn'] 
