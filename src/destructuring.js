const listNumber = [ 1, 2, 3 ];
const [ num1, , num3 ] = listNumber;

console.log(num1); // => 1
console.log(num3); // => 3

const node = {
    parent: "parent",
    child: "child"
};

const { parent, child } = node;

console.log(parent);     // "parent"
console.log(child);      // "child"

// syntax error!
const { item1, item2 };

const tree = {
        node1: "node1",
        node2: "node2"
    },
    tree1 = "tree1",
    tree2 = 5;

// assign different values using destructuring
({ node1, node2 } = tree);

console.log(node1);      // "node1"
console.log(node2);      // "node2"

const product = {
        productType: "Fruit",
        productName: "Apple"
    };

const { productType, productName, productPrice } = product;

console.log(productType);      // "Fruit"
console.log(productName);      // "Apple"
console.log(value);            // undefined

const student = {
        name: "Tom Cruise",
        age: 12
    };

const { name, age, male = true } = student;

console.log(name);      // "Tom Cruise"
console.log(age);       // 12
console.log(male);      // true

const colors = [ "red", "green", "blue" ],
    firstColor = "black",
    secondColor = "purple";

[ firstColor, secondColor ] = colors;

console.log(firstColor);        // "red"
console.log(secondColor);       // "green"
