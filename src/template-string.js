// Simple string substitution
const firstName = 'Teo';
const lastName = 'Nguyen'
console.log(`Hello, ${firstName} ${lastName}!`);
// => Hello, Teo Nguyen!

const a = 10;
const b = 10;
console.log(`JavaScript first appeared ${a+b} years ago. Crazy!`);
//=> JavaScript first appeared 20 years ago. Crazy!

console.log(`The number of JS MVC frameworks is ${2 * (a + b)} and not ${10 * (a + b)}.`);
//=> The number of JS frameworks is 40 and not 200.

const fuction = () => { return 'I am a function'; }
console.log(`Return ${fn()}`);
//=> Return I am a function

const user = {name: 'Caitlin Potter'};
console.log(`Thanks for getting this into V8, ${user.name.toUpperCase()}.`);
// => Thanks for getting this into V8, CAITLIN POTTER

const greeting = `\`Hello\` world!`;
console.log(greeting);
// => `Hello` world!