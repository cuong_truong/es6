var promise1 = new Promise(function (resolve, reject) {
  console.log('\nResolve promise 1');
  resolve('Promise 1 is resolved');
});

var promise2 = new Promise(function (resolve, reject) {
  console.log('\nReject promise 2');
  reject('Promise 2 is rejected');
});

var promise3 = new Promise(function (resolve, reject) {
  console.log('\nError in promise 3');
  throw new Error('This is a big error');
});

promise1.then((value) => {
  console.log('\npromise1 resolved with value: ', value);
},
(error) => {
  console.log('\npromise1 rejected with error: ', error);
});

promise2.then(function (value) {
  console.log('\npromise2 resolved with value: ', value);
});
promise2.catch(function (error) {
  console.log('\npromise2 rejected with error: ', error);
});
promise2.catch(function (error) {
  console.log('\npromise2 rejected with error: ', error, ' in second catch');
});

promise3.then(function (value) {
  console.log('\npromise3 resolved with value: ', value);
});
promise3.catch(function (error) {
  console.log('\npromise3 rejected with error: ', error);
});

var resolvedPromise = Promise.resolve('This promise is resolved quickly');
var rejectedPromise = Promise.reject('This promise is rejected quickly');

resolvedPromise.then((value) => console.log(value));
rejectedPromise.catch((error) => console.log(error));


var chainedPromise = Promise
  .resolve(1)
  .then((value) => {
    console.log(value); // => 1

    return value * 2;
  })
  .then((value) => {
    console.log(value); // => 2

    throw Error(value * 2);
  })
  .catch((error) => {
    console.log(error.message); // => 4

    return Number(error.message) * 2;
  })
  .then((value) => {
    console.log(value); // => 8
  });


const promiseInstant = Promise.resolve('Instant');
const promise100ms = new Promise((resolve, reject) => setTimeout(() => resolve('100'), 100));
const promise500ms = new Promise((resolve, reject) => setTimeout(() => resolve('500'), 500));

const promiseAll = Promise.all([promiseInstant, promise100ms, promise500ms]);
promiseAll.then(value => console.log(value));

const promiseRace = Promise.race([promiseInstant, promise100ms, promise500ms]);
promiseRace.then(value => console.log(value));

async function WaitForPromise() {
  const promiseInstantResult = await promiseInstant;

  console.log('Promise instant result: ', promiseInstantResult);

  return await promise500ms;
};

WaitForPromise().then((value) => {
  console.log('Waited for "', value, '" successfully');
});
