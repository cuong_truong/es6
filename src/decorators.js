function like(target, key, descriptor) {
  const originalMethod = descriptor.value;

  descriptor.value = () => {
    console.log('I love ', key,'!');
    originalMethod();
  };

  return descriptor;
}

const hate = (hateLevel = '') => (target, key, descriptor) => {
  const originalMethod = descriptor.value;

  descriptor.value = () => {
    console.log('I hate ', key,' ', hateLevel);
    originalMethod();
  };

  return descriptor;
};

const evil = (target) => {
  const originalConstructor = target;

  const newConstructor = (...args) => {
    const instance = new originalConstructor(...args);

    instance.color = 'Black';

    return instance;
  };

  newConstructor.isEvil = true;

  return newConstructor;
};

@evil
class SpiderMan {
  constructor() {
    this.color = 'Red';
  }

  @like
  shootString() {
    console.log('Shoot a string');
  }

  @hate('a lot')
  work() {
    console.log('Work as a reporter');
  }
}

const evilSpiderman = new SpiderMan();

console.log('SpiderMan.isEvil = ', SpiderMan.isEvil);
console.log('evilSpiderman.color = ', evilSpiderman.color);
evilSpiderman.shootString();
evilSpiderman.work();
