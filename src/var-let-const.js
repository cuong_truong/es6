function testVar(condition) {
  console.log(color); // => undefined

  if ( condition ) {
    var color = 'blue';

    color = 'black';

    var color = 'grey';
  } else {
     console.log(color); // => undefined

  }

  console.log(color); // => 'grey'

  var funcs = [];

  for (var i = 0; i < 5; i++) {
    funcs.push(function() {
      console.log(i);
    });
  }

  funcs.forEach(function(func) {
    func();
  });
  // => 5
  // => 5
  // => 5
  // => 5
  // => 5
}

function testLet(condition) {
  // console.log(color); // => Uncaught ReferenceError: color is not defined

  if ( condition ) {
    let color = 'blue';

    color = 'black';

    // let color = 'grey'; // => SyntaxError: Identifier 'color' has already been declared
  } else {
    // console.log(color); // => Uncaught ReferenceError: color is not defined
  }

  // console.log(color); // => Uncaught ReferenceError: color is not defined

  let funcs = [];

  for (let i = 0; i < 5; i++) {
    funcs.push(function() {
      console.log(i);
    });
  }

  funcs.forEach(function(func) {
    func();
  });
  // => 0
  // => 1
  // => 2
  // => 3
  // => 4
}

function testConst(condition) {
  console.log(color); // => Uncaught ReferenceError: color is not defined
  if ( condition ) {
    const color = 'blue';
    // color = 'black'; // => TypeError: Assignment to constant variable.
    // const color = 'grey'; // => SyntaxError: Identifier 'color' has already been declared
  } else {
    console.log(color); // => Uncaught ReferenceError: color is not defined
  }
  console.log(color); // => Uncaught ReferenceError: color is not defined

  const pi = 3.1416;
  const radius = 10;
  const circleArea = pi * Math.pow(radius, 2);
  const sphereVolume = (4 / 3) * radius * circleArea;

  console.log("Sphere volume: ", sphereVolume);
}

console.log('Test var');
testVar(true);
console.log('Test let');
testLet(true);
console.log('Test const');
testConst(true);

console.log(__dirname); // => *****/src

var __dirname = 'dir name';
console.log(__dirname); // => 'dir name'

// let __filename = 'file name'; // SyntaxError: Identifier '__filename' has already been declared
// const __filename = 'file name'; // SyntaxError: Identifier '__filename' has already been declared
